#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/unistd.h>

typedef struct point
{
    int isLife;
    int neigh;

} Point;

Point **CreateField(int);
void Print(int, Point **);
void Init(int, Point **, int);
void Neighbours(int, int, int, Point **, int);
void PrintNeig(int, Point **);
void InitNeigh(int, Point **);
void Game(int, Point **);
int isAlive(Point**, int);

int main()
{
    int n=0;
    int state;
    //printf("%d %d", n, sizeof(n));
    while(1){
        if((n<1 || n>100)){
            printf("Enter size of matrix (n x n): ");
            scanf("%d", &n);
        }
        else
            break;
    }
    //printf("If Cliped enter 0, if circular enter 1: ");
    while(1){
        if(state!=0 && state!=1){
            printf("If Cliped enter 0, if circular enter 1: ");
            scanf("%d", &state);
        }
        else
            break;
    }
    printf("\n");
    //system("cls");
    Point **a = CreateField(n);
    while(isAlive(a, n))
    {
        Print(n, a);
        InitNeigh(n, a);
        Init(n, a, state);
        Game(n, a);
        sleep(1);
        //system("clear");
    }
    free(a);
    return 0;
}

/**
 * Creates the field
 * @param[in] n
 * @param[out] a
 */

Point **CreateField(int n)
{
    Point **a;
    srand(time(NULL));
    a = (Point **)malloc(n * sizeof(Point *));
    for (int i = 0; i < n; i++)
        a[i] = (Point *)malloc(n * sizeof(Point));
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            a[i][j].isLife = rand() % 2;
        }
    }
    return a;
}

/**
 * Initiates the neighbours
 * @param[in] n
 * @param[in] a
 */
void InitNeigh(int n, Point **a)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            a[i][j].neigh = 0;
        }
    }
}

/**
 * Prints the field
 * @param[in] n
 * @param[in] a
 */
void Print(int n, Point **a)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("%d ", a[i][j].isLife);
        }
        printf("\n");
    }
    printf("\n");
}

/**
 *
 * @param[in] n
 * @param[in] a
 * @param[in] state
 */
void Init(int n, Point **a, int state)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            Neighbours(i, j, n, a, state);
        }
    }
}

/**
 *
 * @param[in] i
 * @param[in] j
 * @param[in] n
 * @param[in] a
 * @param[in] state
 */
void Neighbours(int i, int j, int n, Point **a, int state)
{
    for (int k = i - 1; k <= i + 1; k++)
    {
        for (int t = j - 1; t <= j + 1; t++)
        {
            if(state){
                if (k == i && t == j)
                {
                    continue;
                }
                else if (a[(k % n + n) % n][(t % n + n) % n].isLife == 1)
                {
                    a[i][j].neigh++;
                }
            }
            else{
                if (k<0 || t<0 || (k == i && t == j) || k>n-1 || t>n-1)
                {
                    continue;
                }
                else if (a[k][t].isLife == 1)
                {
                    a[i][j].neigh++;
                }
            }
        }
    }
}

/**
 *
 * @param[in] n
 * @param[in] a
 */
void PrintNeig(int n, Point **a)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("%d ", a[i][j].neigh);
        }
        printf("\n");
    }
}

/**
 *
 * @param[in] n
 * @param[in] a
 */
void Game(int n, Point **a)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (a[i][j].isLife == 0 && a[i][j].neigh == 3)
                a[i][j].isLife = 1;
            else if (a[i][j].isLife == 1 && (a[i][j].neigh > 3 || a[i][j].neigh < 2))
                a[i][j].isLife = 0;
        }
    }
}

/**
 *
 * @param[in] a
 * @param[in] n
 * @return
 */
int isAlive(Point** a, int n){
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if(a[i][j].isLife!=0){
                return 1;
            }

        }
    }
    return 0;
}